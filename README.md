My Hearing Center is your center for better hearing in Jensen and West Palm Beach. A full array of professional audiology services is available for seasonal and full-time residents. From hearing evaluations to cleaning and repairing hearing aids, we meet your needs conveniently in your neighborhood.

Address: 1742 S Congress Ave, Palm Springs, FL 33461, USA

Phone: 772-232-2613

Website: https://myhearingcenter.net